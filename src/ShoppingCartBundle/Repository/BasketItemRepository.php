<?php

namespace ShoppingCartBundle\Repository;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\TransactionRequiredException;
use ShoppingCartBundle\Entity\BasketItem;
use ShoppingCartBundle\Entity\ProductSize;
use UserBundle\Entity\User;

/**
 * Class BasketItemRepository
 *
 * @package ShoppingCartBundle\Repository
 */
class BasketItemRepository extends EntityRepository
{
    /**
     * Adds the current concrete product and the selected size to the basketItem table.
     * If a record with the same user and size already exists, it updates it by adding +1
     * to the quantity.
     *
     * @param ProductSize $size
     * @param User $user
     * @param int $quantity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addToBasket(ProductSize $size, User $user, $quantity = 1)
    {
        $queryBuilder = $this->createQueryBuilder('bi');

        $basketItems = $queryBuilder
            ->where('bi.size =:size')
            ->andWhere('bi.user =:user')
            ->setParameter('size', $size)
            ->setParameter('user', $user)
            ->getQuery()->getResult();

        if(count($basketItems) == 1) {
            /** @var BasketItem $basketItem */
            $basketItem = $basketItems[0];
            $quantity = $basketItem->getQuantity() + 1;
            $queryBuilder
                ->update('ShoppingCartBundle:BasketItem', 'bi')
                ->set('bi.quantity', ':qty')
                ->where('bi.user =:user')
                ->andWhere('bi.size =:size')
                ->setParameter('user', $user)
                ->setParameter('size', $size)
                ->setParameter('qty', $quantity)
                ->getQuery()->execute();
        } else {
            /** @var BasketItem $basketItem */
            $basketItem = new BasketItem($size, $user, $quantity);

            $this->getEntityManager()->persist($basketItem);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Gets all the basket items per user
     *
     * @param User $user
     *
     * @return Query
     */
    public function queryBasketPerUser(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('bi');
        $queryBuilder
            ->select('cp.id as product, pr.name, cp.price, cp.colour, pi.image, ps.size, bi.quantity, bi.id')
            ->from('ShoppingCartBundle:ProductImage', 'pi')
            ->from('ShoppingCartBundle:ProductSize', 'ps')
            ->from('ShoppingCartBundle:ConcreteProduct', 'cp')
            ->from('ShoppingCartBundle:Product', 'pr')
            ->where('bi.user =:user')
            ->andWhere('bi.size = ps.id')
            ->andWhere('ps.concreteProduct = cp.id')
            ->andWhere('cp.product = pr.id')
            ->andWhere('cp.defaultImage = pi.id')
            ->setParameter('user', $user);

        return $queryBuilder->getQuery();
    }

    /**
     * Gets all basket items per User and their corresponding quantity in stock
     *
     * @param User $user
     *
     * @return array
     */
    public function getBasketAndStockQuantity(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('bi');
        $query = $queryBuilder
            ->select('ps.id, ps.quantity, bi as basketItem')
            ->from('ShoppingCartBundle:ProductSize', 'ps')
            ->where('bi.size = ps.id')
            ->andWhere('bi.user =:user')
            ->setParameter('user', $user)
            ->getQuery();

        try {
            $query->setLockMode(LockMode::PESSIMISTIC_WRITE);
        } catch (TransactionRequiredException $e) {

        }

        return $query->getResult();
    }

    /**
     * Delete the specified users' basket after checkout
     *
     * @param User $user
     */
    public function deleteBasket(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('bi');
        $queryBuilder
            ->delete()
            ->where('bi.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    /**
     * Delete the basketItem with the specified id
     *
     * @param $id   -   The basketItem id to be deleted
     */
    public function deleteItemFromBasket($id)
    {
        $queryBuilder = $this->createQueryBuilder('bi');
        $queryBuilder
            ->delete()
            ->where('bi.id =:id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    /**
     * Counts and returns the basket items per User
     *
     * @param User $user
     *
     * @return int
     */
    public function countBasketItemsPerUser(User $user)
    {
        $query = $this->queryBasketPerUser($user);

        return count($query->getResult());
    }
}
