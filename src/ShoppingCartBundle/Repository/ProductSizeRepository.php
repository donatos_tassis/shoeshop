<?php

namespace ShoppingCartBundle\Repository;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\TransactionRequiredException;

/**
 * Class ProductSizeRepository
 *
 * @package ShoppingCartBundle\Repository
 */
class ProductSizeRepository extends EntityRepository
{

    /**
     * Updates the stock in each product after successful checkout
     *
     * @param $id
     * @param $quantity
     */
    public function updateProductsQuantityInStock($id, $quantity)
    {
        $queryBuilder = $this->createQueryBuilder('ps');
        $query = $queryBuilder
            ->update()
            ->set('ps.quantity', ':qty')
            ->where('ps.id =:id')
            ->setParameter('id', $id)
            ->setParameter('qty', $quantity)
            ->getQuery();

        try {
            $query->setLockMode(LockMode::PESSIMISTIC_WRITE);
        } catch (TransactionRequiredException $e) {

        }

        $query->execute();
    }
}
