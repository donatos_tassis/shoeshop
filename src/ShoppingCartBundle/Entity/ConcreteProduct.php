<?php

namespace ShoppingCartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConcreteProduct
 *
 * @ORM\Table(name="concrete_product")
 * @ORM\Entity(repositoryClass="ShoppingCartBundle\Repository\ConcreteProductRepository")
 */
class ConcreteProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="ShoppingCartBundle\Entity\Product", inversedBy="concreteProducts")
     * @ORM\JoinColumn(name="product", referencedColumnName="id", nullable=false)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="colour", type="string", length=255)
     */
    private $colour;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var ProductImage
     * @ORM\OneToOne(targetEntity="ShoppingCartBundle\Entity\ProductImage")
     * @ORM\JoinColumn(name="default_image", referencedColumnName="id", nullable=false)
     */
    private $defaultImage;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ShoppingCartBundle\Entity\ProductImage", mappedBy="concreteProduct")
     */
    private $images;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ShoppingCartBundle\Entity\ProductSize", mappedBy="concreteProduct")
     */
    private $sizes;

    /**
     * ConcreteProduct constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->sizes = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product.
     *
     * @param int $product
     *
     * @return ConcreteProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set colour.
     *
     * @param string $colour
     *
     * @return ConcreteProduct
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour.
     *
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ConcreteProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return ConcreteProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get images for this concrete product
     *
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    /**
     * @return ProductImage
     */
    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * @param ProductImage $defaultImage
     */
    public function setDefaultImage($defaultImage)
    {
        $this->defaultImage = $defaultImage;
    }

    /**
     * Adds an image to the images list of this ConcreteProduct
     *
     * @param ProductImage $image
     *
     * @return ConcreteProduct
     */
    public function addImage(ProductImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Removes an image from the images list.
     *
     * @param ProductImage $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImage(ProductImage $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get sizes for this concrete product
     *
     * @return ArrayCollection
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * Adds a size to the sizes list of this ConcreteProduct
     *
     * @param ProductSize $size
     *
     * @return ConcreteProduct
     */
    public function addSize(ProductSize $size)
    {
        $this->sizes[] = $size;

        return $this;
    }

    /**
     * Removes a size from the sizes list.
     *
     * @param ProductSize $size
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSize(ProductSize $size)
    {
        return $this->sizes->removeElement($size);
    }
}
