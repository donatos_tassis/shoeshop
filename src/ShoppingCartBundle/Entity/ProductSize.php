<?php

namespace ShoppingCartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductSize
 *
 * @ORM\Table(name="product_size")
 * @ORM\Entity(repositoryClass="ShoppingCartBundle\Repository\ProductSizeRepository")
 */
class ProductSize
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ConcreteProduct
     * @ORM\ManyToOne(targetEntity="ShoppingCartBundle\Entity\ConcreteProduct", inversedBy="sizes")
     * @ORM\JoinColumn(name="concrete_product", referencedColumnName="id", nullable=false)
     */
    private $concreteProduct;

    /**
     * @var float
     *
     * @ORM\Column(name="size", type="float")
     */
    private $size;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concreteProduct.
     *
     * @param int $concreteProduct
     *
     * @return ProductSize
     */
    public function setConcreteProduct($concreteProduct)
    {
        $this->concreteProduct = $concreteProduct;

        return $this;
    }

    /**
     * Get concreteProduct.
     *
     * @return ConcreteProduct
     */
    public function getConcreteProduct()
    {
        return $this->concreteProduct;
    }

    /**
     * Set size.
     *
     * @param float $size
     *
     * @return ProductSize
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return ProductSize
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
