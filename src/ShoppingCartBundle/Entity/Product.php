<?php

namespace ShoppingCartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="ShoppingCartBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="ShoppingCartBundle\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ShoppingCartBundle\Entity\ConcreteProduct", mappedBy="product")
     */
    protected $concreteProducts;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->concreteProducts = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category.
     *
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category.
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get concrete products for this product
     *
     * @return ArrayCollection
     */
    public function getConcreteProducts()
    {
        return $this->concreteProducts;
    }

    /**
     * Adds a concrete product to the products list of this Product
     *
     * @param ConcreteProduct $concreteProduct
     *
     * @return Product
     */
    public function addConcreteProduct(ConcreteProduct $concreteProduct)
    {
        $this->concreteProducts[] = $concreteProduct;

        return $this;
    }

    /**
     * Removes a concrete product.
     *
     * @param ConcreteProduct $concreteProduct
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConcreteProduct(ConcreteProduct $concreteProduct)
    {
        return $this->concreteProducts->removeElement($concreteProduct);
    }
}
