<?php

namespace ShoppingCartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductImage
 *
 * @ORM\Table(name="product_image")
 * @ORM\Entity(repositoryClass="ShoppingCartBundle\Repository\ProductImageRepository")
 */
class ProductImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ConcreteProduct
     * @ORM\ManyToOne(targetEntity="ShoppingCartBundle\Entity\ConcreteProduct", inversedBy="images")
     * @ORM\JoinColumn(name="concrete_product", referencedColumnName="id", nullable=false)
     */
    private $concreteProduct;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Image()
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concreteProduct.
     *
     * @param int $concreteProduct
     *
     * @return ProductImage
     */
    public function setConcreteProduct($concreteProduct)
    {
        $this->concreteProduct = $concreteProduct;

        return $this;
    }

    /**
     * Get concreteProduct.
     *
     * @return ConcreteProduct
     */
    public function getConcreteProduct()
    {
        return $this->concreteProduct;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return ProductImage
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}
