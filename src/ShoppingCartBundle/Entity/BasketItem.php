<?php

namespace ShoppingCartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * BasketItem
 *
 * @ORM\Table(name="basket_item")
 * @ORM\Entity(repositoryClass="ShoppingCartBundle\Repository\BasketItemRepository")
 */
class BasketItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ProductSize
     * @ORM\ManyToOne(targetEntity="ShoppingCartBundle\Entity\ProductSize")
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id", nullable=false)
     */
    private $size;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * BasketItem constructor.
     * @param ProductSize $size
     * @param User $user
     * @param int $quantity
     */
    public function __construct($size, $user, $quantity)
    {
        $this->setUser($user);
        $this->setSize($size);
        $this->setQuantity($quantity);
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set size.
     *
     * @param ProductSize $size
     *
     * @return BasketItem
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return ProductSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return BasketItem
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return BasketItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
