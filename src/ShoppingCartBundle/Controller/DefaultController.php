<?php

namespace ShoppingCartBundle\Controller;

use Knp\Component\Pager\Paginator;
use ShoppingCartBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * Home page with all products
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Product::class)->getDefaultProductsQuery();
        $query->setHint('knp_paginator.count', $query->getScalarResult());

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $products = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            9,
            [
                'distinct' => false
            ]
        );

        foreach ($products as $i => $product) {
            $product['price'] = sprintf('%0.2f',$product['price'] / 100);
            $products[$i] = $product;
        }

        return $this->render(
            'ShoppingCartBundle:Default:index.html.twig',
            [
                'products' => $products
            ]
        );
    }
}
