<?php

namespace ShoppingCartBundle\Controller;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use http\Exception;
use ShoppingCartBundle\Entity\BasketItem;
use ShoppingCartBundle\Entity\ConcreteProduct;
use ShoppingCartBundle\Entity\ProductSize;
use ShoppingCartBundle\Helper\SizeConverterHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

class ProductController extends Controller
{
    /** @const string  */
    const PRODUCT_NOT_FOUND = "Product not found";
    const PRODUCT_SIZE_NOT_AVAILABLE = 'Product size not available';

    /**
     * Loads the product page with relevant information needed
     *
     * @param $id -  the id of the product
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function productAction($id) {
        $em = $this->getDoctrine()->getManager();
        $concreteProductRepo = $em->getRepository(ConcreteProduct::class);

        /** @var ConcreteProduct $product */
        $product = $concreteProductRepo->find($id);
        if(!$product)
            throw new InvalidArgumentException(self::PRODUCT_NOT_FOUND);

        $productInfo = $concreteProductRepo->getConcreteProductInfo($id);
        $productInfo['price'] = sprintf('%0.2f',$productInfo['price'] / 100);

        $productImages = $concreteProductRepo->getConcreteProductImages($id);
        $productSizes = $concreteProductRepo->getConcreteProductSizes($id);
        $productColours = $concreteProductRepo->getConcreteProductColours($product->getProduct()->getId());

        return $this->render(
            '@ShoppingCart/Product/product_index.html.twig',
            [
                'info' => $productInfo,
                'images' => $productImages,
                'sizes' => $productSizes,
                'colours' => $productColours,
                'sizeGuide' => SizeConverterHelper::ADULTS_SIZES_UK_EU,
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addBasketAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if(!$user) {
            // redirect to login
            return new JsonResponse('route::'.$this->get('router')->generate('fos_user_security_login'));
        }

        $filters = $request->get('filters', []);
        $sizeId = (int)$filters['size'];
        /** @var ProductSize $productSize */
        $productSize = $this->getDoctrine()->getRepository(ProductSize::class)->find($sizeId);
        if(!$productSize)
            throw new InvalidArgumentException(self::PRODUCT_SIZE_NOT_AVAILABLE);

        $this->getDoctrine()->getRepository(BasketItem::class)->addToBasket($productSize, $user);

        try {
            $content = json_encode([]);
            $status = empty($content) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK;
        } catch (Exception $exception) {
            $content = json_encode(array('error' => $exception->getMessage()));
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($content);
        $response->setStatusCode($status);

        return $response;
    }

    /**
     * Deletes the basketItem with the specified id
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeBasketAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if(!$user) {
            // redirect to login
            return new JsonResponse('route::'.$this->get('router')->generate('fos_user_security_login'));
        }

        $filters = $request->get('filters', []);
        $basketId = (int)$filters['basketId'];
        /** @var BasketItem $basketItem */
        $basketItem = $this->getDoctrine()->getRepository(BasketItem::class)->find($basketId);
        if($basketItem->getUser() !== $user)
            return new JsonResponse('Unauthorised action', Response::HTTP_UNAUTHORIZED);

        $this->getDoctrine()->getRepository(BasketItem::class)->deleteItemFromBasket($basketId);

        return new JsonResponse('route::'.$this->get('router')->generate('view_basket'));
    }
}
