<?php

namespace ShoppingCartBundle\Controller;

use Knp\Component\Pager\Paginator;
use ShoppingCartBundle\Entity\BasketItem;
use ShoppingCartBundle\Entity\ProductSize;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 * Class BasketController
 *
 * @package ShoppingCartBundle\Controller
 */
class BasketController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function viewBasketAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if(!$user) {
            // redirect to login
            return $this->redirectToRoute('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(BasketItem::class)->queryBasketPerUser($user);
        $count = $em->getRepository(BasketItem::class)->countBasketItemsPerUser($user);
        $query->setHint('knp_paginator.count', $count);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $basketItems = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10,
            [
                'distinct' => false,
            ]
        );

        $totalAmount = 0;
        foreach ($basketItems as $basketItem) {
            $totalAmount += ($basketItem['price'] * $basketItem['quantity']);
        }

        $notInStock = $request->query->get('notInStock');

        return $this->render(
            'ShoppingCartBundle:BasketController:view_basket.html.twig',
            [
                'items' => $basketItems,
                'totalAmount' => $totalAmount,
                'notInStock' => $notInStock,
            ]
        );
    }

    /**
     * @return JsonResponse|Response
     */
    public function checkoutAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        if(!$user) {
            // redirect to login
            return new JsonResponse('route::'.$this->get('router')->generate('fos_user_security_login'));
        }

        $basketRepo = $this->getDoctrine()->getRepository(BasketItem::class);
        $results = $basketRepo->getBasketAndStockQuantity($user);

        $notEnoughStock = [];
        $stockItems = [];
        foreach ($results as $result) {
            /** @var BasketItem $basketItem */
            $basketItem = $result['basketItem'];

            if($basketItem->getQuantity() > $result['quantity'])
                $notEnoughStock[$basketItem->getId()] = $result['quantity'];

            $item = [
                'id' => $result['id'],
                'quantity' => $result['quantity'] - $basketItem->getQuantity(),
            ];

            array_push($stockItems, $item);
        }

        if (!empty($notEnoughStock)) {
            $this->addFlash('out-of-stock', 'There are Out of Stock items in your basket');
            return new JsonResponse('route::'.$this->get('router')->generate(
                'view_basket',
                    [
                        'notInStock' => $notEnoughStock,
                    ]
                ));
        } else {
            $this->addFlash('success', 'Checkout completed successfully');
            $productSizeRepo = $this->getDoctrine()->getRepository(ProductSize::class);

            foreach ($stockItems as $stockItem) {
                $productSizeRepo->updateProductsQuantityInStock(
                    $stockItem['id'],
                    $stockItem['quantity']
                );
            }

            $basketRepo->deleteBasket($user);
            return new JsonResponse('route::'.$this->get('router')->generate('view_basket'));
        }
    }
}
