<?php

namespace ShoppingCartBundle\Helper;

/**
 * Class SizeConverterHelper
 *
 * @package ShoppingCartBundle\Helper
 */
class SizeConverterHelper
{
    /** @const array  */
    const ADULTS_SIZES_UK_EU = [
        [
            'uk' => 2,
            'eu' => 35,
        ],
        [
            'uk' => 2.5,
            'eu' => 35.5,
        ],
        [
            'uk' => 3,
            'eu' => 36,
        ],
        [
            'uk' => 3.5,
            'eu' => 36.5,
        ],
        [
            'uk' => 4,
            'eu' => 37,
        ],
        [
            'uk' => 4.5,
            'eu' => 37.5,
        ],
        [
            'uk' => 5,
            'eu' => 38,
        ],
        [
            'uk' => 5.5,
            'eu' => 38.5,
        ],
        [
            'uk' => 6,
            'eu' => 39,
        ],
        [
            'uk' => 6.5,
            'eu' => 40,
        ],
        [
            'uk' => 7,
            'eu' => 41,
        ],
        [
            'uk' => 7.5,
            'eu' => 41.5,
        ],
        [
            'uk' => 8,
            'eu' => 42,
        ],
        [
            'uk' => 8.5,
            'eu' => 42.5,
        ],
        [
            'uk' => 9,
            'eu' => 43,
        ],
        [
            'uk' => 9.5,
            'eu' => 44,
        ],
        [
            'uk' => 10,
            'eu' => 45,
        ],
        [
            'uk' => 10.5,
            'eu' => 45.5,
        ],
        [
            'uk' => 11,
            'eu' => 46,
        ],
        [
            'uk' => 11.5,
            'eu' => 46.5,
        ],
        [
            'uk' => 12,
            'eu' => 47,
        ],
        [
            'uk' => 12.5,
            'eu' => 47.5,
        ],
        [
            'uk' => 13,
            'eu' => 48,
        ],
    ];
}