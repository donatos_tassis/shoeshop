<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;


class RegistrationFormType extends AbstractType
{

    /**
     * builds the registration form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('surname')
            ->remove('username');
    }

    /**
     * Extending the parent registration form to add/remove more fields
     *
     * @return mixed
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}