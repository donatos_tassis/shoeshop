const checkoutButton = document.querySelector('#checkout-button');
checkoutButton.addEventListener('click', checkout);

function checkout() {
    $.ajax({
        url:'/checkout',
        type: "POST",
        dataType: "json",
        async: true,
        success: function(html)
        {
            if (html.startsWith('route::')) {
                route = html.replace('route::', '');
                location.href = route;
            }
        }
    });
}