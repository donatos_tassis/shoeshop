const current = document.querySelector('#current');
const images = document.querySelector('.product-page-images');
const image = document.querySelectorAll('.product-page-images img');
const opacity = 0.6;

image[0].style.opacity = opacity;
images.addEventListener('click', imageClick);

function imageClick(event) {
    if(!event.target.matches(`img`)) return;

    // Reset the opacity
    image.forEach(image => (image.style.opacity = 1));

    // Change current image to src of clicked image
    current.src = event.target.src;

    // Add fade in class
    current.classList.add('fade-in');

    // Remove fade-in class after 0.25 seconds
    setTimeout(() => current.classList.remove('fade-in'), 250);

    // Change the opacity to opacity var
    event.target.style.opacity = opacity;
}