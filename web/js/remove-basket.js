const basketItems = document.querySelector('.basket-items');
const removeButton = document.querySelectorAll('.basket-items div div label input');

basketItems.addEventListener('click', removeBasketItem);

function removeBasketItem(event) {
    if(!event.target.matches(`input`)) return;

    var basketId = $(event.target).closest('label').find('span#basket-id').text();

    $.ajax({
        url:'/remove-from-basket',
        type: "POST",
        dataType: "json",
        data: {
            filters : {
                basketId : basketId
            }
        },
        async: true,

        success: function(html)
        {
            if (html.startsWith('route::')) {
                route = html.replace('route::', '');
                location.href = route;
            }
        }
    });
}