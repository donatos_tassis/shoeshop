const sizes = document.querySelector('.sizes');
const size = document.querySelectorAll('.sizes label span');

sizes.addEventListener('click', sizeClick);

function sizeClick(event) {
    if(!event.target.matches(`span`)) return;

    // Reset the color of all size buttons
    size.forEach(function(element) {
        element.style.background = "#ffffff";
        element.style.color = "#000000";
    });

    event.target.style.background = "#000000";
    event.target.style.color = "#ffffff";
}

const addButton = document.querySelector('#add-basket');
addButton.addEventListener('click', addToBasket);

function addToBasket(event) {
    var checkedSize = $('input[name=selected-size]:checked', '.sizes');
    var sizeId = checkedSize.closest('label').find('span#size-id').text();

    $.ajax({
        url:'/add-basket',
        type: "POST",
        dataType: "json",
        data: {
            filters : {
                size : sizeId
            }
        },
        async: true,

        success: function(html)
        {
            if (html.startsWith('route::')) {
                route = html.replace('route::', '');
                location.href = route;
            }
        }
    });
}