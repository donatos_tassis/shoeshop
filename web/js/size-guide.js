$(document).ready(function () {
    $("#size-guide").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss-guide, .overlay').on('click', function () {
        // hide size-guide
        $('#size-guide').removeClass('active');
        // hide size-overlay
        $('.overlay').removeClass('active');
    });

    $('#sizeGuideCollapse').on('click', function () {
        // open side-guide
        $('#size-guide').addClass('active');
        // fade in the size-overlay
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});

mainContent = document.getElementById('mainContent');
sizeGuide = document.getElementById('sizeGuide');
