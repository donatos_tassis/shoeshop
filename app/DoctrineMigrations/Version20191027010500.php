<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191027010500 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE basket_item (id INT AUTO_INCREMENT NOT NULL, size_id INT NOT NULL, user INT NOT NULL, quantity INT NOT NULL, INDEX IDX_D4943C2B498DA827 (size_id), INDEX IDX_D4943C2B8D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B498DA827 FOREIGN KEY (size_id) REFERENCES product_size (id)');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B8D93D649 FOREIGN KEY (user) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE concrete_product ADD CONSTRAINT FK_DC7F66C62C1B5051 FOREIGN KEY (default_image) REFERENCES product_image (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC7F66C62C1B5051 ON concrete_product (default_image)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE basket_item');
        $this->addSql('ALTER TABLE concrete_product DROP FOREIGN KEY FK_DC7F66C62C1B5051');
        $this->addSql('DROP INDEX UNIQ_DC7F66C62C1B5051 ON concrete_product');
    }
}
