<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021192027 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE concrete_product ADD description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE product DROP description, DROP brand, DROP model');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE concrete_product DROP description');
        $this->addSql('ALTER TABLE product ADD description LONGTEXT NOT NULL COLLATE utf8_unicode_ci, ADD brand VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD model VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
