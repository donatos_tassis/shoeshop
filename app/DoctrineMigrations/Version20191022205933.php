<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191022205933 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE concrete_product ADD default_image INT NOT NULL');
        $this->addSql('ALTER TABLE concrete_product ADD CONSTRAINT FK_DC7F66C664617F03 FOREIGN KEY (default_image) REFERENCES product_image (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC7F66C664617F03 ON concrete_product (default_image)');
        $this->addSql('ALTER TABLE product_image DROP is_default');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE concrete_product DROP FOREIGN KEY FK_DC7F66C664617F03');
        $this->addSql('DROP INDEX UNIQ_DC7F66C664617F03 ON concrete_product');
        $this->addSql('ALTER TABLE concrete_product DROP default_image');
        $this->addSql('ALTER TABLE product_image ADD is_default TINYINT(1) NOT NULL');
    }
}
