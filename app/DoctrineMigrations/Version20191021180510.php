<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021180510 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE concrete_product (id INT AUTO_INCREMENT NOT NULL, product INT NOT NULL, colour VARCHAR(255) NOT NULL, price INT NOT NULL, INDEX IDX_DC7F66C6D34A04AD (product), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, grouping VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, image VARCHAR(255) NOT NULL, price INT NOT NULL, brand VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, INDEX IDX_D34A04AD64C19C1 (category), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_image (id INT AUTO_INCREMENT NOT NULL, concrete_product INT NOT NULL, image VARCHAR(255) NOT NULL, INDEX IDX_64617F03DC7F66C6 (concrete_product), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_size (id INT AUTO_INCREMENT NOT NULL, concrete_product INT NOT NULL, quantity INT NOT NULL, INDEX IDX_7A2806CBDC7F66C6 (concrete_product), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE concrete_product ADD CONSTRAINT FK_DC7F66C6D34A04AD FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD64C19C1 FOREIGN KEY (category) REFERENCES category (id)');
        $this->addSql('ALTER TABLE product_image ADD CONSTRAINT FK_64617F03DC7F66C6 FOREIGN KEY (concrete_product) REFERENCES concrete_product (id)');
        $this->addSql('ALTER TABLE product_size ADD CONSTRAINT FK_7A2806CBDC7F66C6 FOREIGN KEY (concrete_product) REFERENCES concrete_product (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_image DROP FOREIGN KEY FK_64617F03DC7F66C6');
        $this->addSql('ALTER TABLE product_size DROP FOREIGN KEY FK_7A2806CBDC7F66C6');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD64C19C1');
        $this->addSql('ALTER TABLE concrete_product DROP FOREIGN KEY FK_DC7F66C6D34A04AD');
        $this->addSql('DROP TABLE concrete_product');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_image');
        $this->addSql('DROP TABLE product_size');
    }
}
